package com.minimalart.chatui.activities;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.minimalart.chatui.R;
import com.minimalart.chatui.adapters.MessagesAdapter;
import com.minimalart.chatui.models.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class ChatActivity extends AppCompatActivity {

    private static final String TAG = ChatActivity.class.getSimpleName();

    private RecyclerView messagesRecyclerView;
    private TextView receiver;
    private ImageButton backBtn;
    private ImageButton infoBtn;
    private ImageButton sendBtn;
    private EditText messageContent;
    private MessagesAdapter adapter;

    private ArrayList<Message> listMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        setViews();
    }

    private void setViews() {

        messagesRecyclerView = (RecyclerView) findViewById(R.id.rv_chat);

        backBtn = (ImageButton) findViewById(R.id.btnBack);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        infoBtn = (ImageButton) findViewById(R.id.btnInfo);
        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seeReceiverInfo();
            }
        });

        sendBtn = (ImageButton) findViewById(R.id.btnSendMessage);

        messageContent = (EditText) findViewById(R.id.messageText);
        messageContent.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf"));

        receiver = (TextView) findViewById(R.id.name_receiver);
        receiver.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/roboto_regular.ttf"));

        listMessages = new ArrayList<>();
        addMsgs();
        adapter = new MessagesAdapter(listMessages, getBaseContext());
        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOnClick();
            }
        });
        adapter.setLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                handleLongClick();
                return true;
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        manager.setStackFromEnd(true);
        manager.setSmoothScrollbarEnabled(true);

        messagesRecyclerView.setLayoutManager(manager);
        messagesRecyclerView.setAdapter(adapter);
        messagesRecyclerView.scrollToPosition(0);
        messagesRecyclerView.addItemDecoration(new RVItemDecoration(32));
        messagesRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                Log.v(TAG, "onLayoutChange");
                messagesRecyclerView.scrollToPosition(adapter.getItemCount()-1);
            }
        });
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
        messagesRecyclerView.scrollToPosition(messagesRecyclerView.getAdapter().getItemCount()-1);
    }

    private void handleLongClick() {
        //TODO: Handle long click events on messages
        Log.v(TAG, "long click");
    }

    private void handleOnClick() {
        //TODO: Handle click events on messages
        Log.v(TAG, "click");
    }

    private void seeReceiverInfo() {
        //TODO: Open info about the person client talks to
        Log.v(TAG, "info");
    }

    private void sendMessage() {
        adapter.addMessage(new Message(messageContent.getText().toString(), "usr1", "usr2", "15:00", 1));
        messagesRecyclerView.scrollToPosition(adapter.getItemCount()-1);
    }

    private void closeKeyboard(int id) {
        View view = this.getCurrentFocus();

        if(id == 1) {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private void addMsgs() {
        for(int i=0; i<100; i++) {
            listMessages.add(new Message(UUID.randomUUID() + " " + String.valueOf(i), "usr1", "usr2", String.valueOf(new Date().getHours()) + ":" + String.valueOf(new Date().getMinutes()), 1));
            listMessages.add(new Message(UUID.randomUUID() + " " + String.valueOf(i), "usr2", "usr1", String.valueOf(new Date().getHours()) + ":" + String.valueOf(new Date().getMinutes()), 1));
        }
    }
}

class RVItemDecoration extends RecyclerView.ItemDecoration {

    private int margin;

    public RVItemDecoration(int margin) {
        this.margin = margin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int position = parent.getChildLayoutPosition(view);

        if(position == 0)
            outRect.top = margin;

        outRect.right = margin;
        outRect.left = margin;
        outRect.bottom = margin;
    }
}
