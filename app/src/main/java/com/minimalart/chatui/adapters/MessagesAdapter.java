package com.minimalart.chatui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minimalart.chatui.R;
import com.minimalart.chatui.models.Message;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ytgab on 7/17/2017.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder> {

    public static final int TYPE_RECEIVED = 1;
    public static final int TYPE_SENT = 0;

    private View.OnClickListener onClickListener;
    private View.OnLongClickListener onLongClickListener;

    private Context context;

    private String TAG = MessagesAdapter.class.getSimpleName();
    private ArrayList<Message> messages;

    public MessagesAdapter(ArrayList<Message> messages, Context context) {
        this.messages = messages;
        this.context = context;
    }

    @Override
    public MessagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int res = 0;
        switch (viewType){
            case TYPE_RECEIVED:
                res = R.layout.message_receiver;
                break;
            case TYPE_SENT:
                res = R.layout.message_sender;
                break;
        }

        View messageView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
        messageView.setOnClickListener(onClickListener);
        messageView.setOnLongClickListener(onLongClickListener);

        return new MessagesViewHolder(messageView);
    }

    public void setClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    public void addMessage(Message message){
        messages.add(message);
        notifyItemInserted(messages.size()-1);
    }

    @Override
    public void onBindViewHolder(MessagesViewHolder holder, int position) {
        Message message = messages.get(position);
        holder.updateUI(message);
    }

    @Override
    public int getItemViewType(int position) {
        if(messages.get(position).getReceiver().equals("usr1"))
            return 1;
        else
            return 0;
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MessagesViewHolder extends RecyclerView.ViewHolder{

        private TextView text;
        private TextView timestamp;
        private CircleImageView profilePic;

        public MessagesViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.message_content);
            text.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_light.ttf"));
            timestamp = (TextView) itemView.findViewById(R.id.txt_time);
            timestamp.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_light.ttf"));
            profilePic = (CircleImageView) itemView.findViewById(R.id.bg_person);
        }

        public void updateUI(Message message) {
            text.setText(message.getText());
            timestamp.setText(message.getTimestamp());
        }

    }
}
